x = 5
print(x, end=" aqui me gustaría una nueva linea")
print(x)
y = 100
z = 400
print(x, y, z, sep=" separado de ", end=" aqui me gustaría una nueva linea")
print(x)

print("El valor de x es", x, sep="")
print("El valor de x es " + str(x))
print(f"el valor de x es     {x}")

# if - elif - else

print(y < 100)

if y == 100:
    print("igual")
if y > 100:
    print("mayor")
else:
    print("menor")

print(x)

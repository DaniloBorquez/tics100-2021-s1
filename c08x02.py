"""import math
print(math.sqrt(math.pow(0.5-math.sin(math.pi/3), 4) +
      math.pow(0.5+math.cos(-math.pi/4), 6)))
"""


def suma(x, y):
    print(f"La suma es {x+y}!")


def listaConSuma(x, y, z):
    return [x, y, z, x+y+z]


a = 100
b = 200
suma(a, b)

A = 10
B = 4+5j
C = 20.5
lista = listaConSuma(A, B, C)
print(lista)

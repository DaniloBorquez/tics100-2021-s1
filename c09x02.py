string = "conjunto de caracteres"
print(string)
print(string[4])
print(string[4:6])
print(string[4:12:2])

string = "Hola a todo el mundo!!!"
print(string)
string = string + "programación"
print(string)
numeros = ""
for i in range(10):
    numeros += str(i)+" "  # numeros = numeros + str(i) + " "
print(numeros)

lista = []
print(lista)
lista.append(2)
lista.append(4)
lista.append("asd")
lista.append([2, 5])
print(lista)
print(lista[1:3])
lista.extend([4, 5, 6, 7, [3, 4]])
print(lista)
lista += ["sumando", "otra", "lista"]
print(lista)
lista[3] = "he cambiado el elemento en la posición 3"
print(lista)
print(lista[-1])
for i in range(1, len(lista)+1):
    print(lista[-i], len(lista), sep="-", end=" ")
print()

print(lista[2])
palabra = lista[2]
print(palabra[1])
print(lista[2][1])
print(len(lista[2]))


nueva_lista = []
for i in range(10):
    nueva_lista.append(i)
print(nueva_lista)
for i in nueva_lista:
    print(i, end=" ")
print()

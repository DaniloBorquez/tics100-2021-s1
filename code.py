x = 6
if x == 7:
    print("x es igual a 7!")
    print("Dentro del bloque if")
elif x < 7:
    print("x es menor a 7")
    print("Dentro de bloque else-if")
else:
    print("x es mayor a 7")
    print("Dentro de bloque else-else")
# else:
#    if x < 7:
#        print("x es menor a 7")
#        print("Dentro de bloque else-if")
#    else:
#        print("x es mayor a 7")
#        print("Dentro de bloque else-else")
print("Fuera del bloque")

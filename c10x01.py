palabra = "Hola a todos"

cont = 1
for letra in palabra:
    print(cont, letra)
    cont += 1

for i in range(len(palabra)):
    print(i, palabra[i], sep=":", end=" ")
print()

suma = 0
for i in range(1, 1001):
    suma += i
print(suma)
opcion = 9876
count = 0
while opcion != 2:
    count += 1
    print("""Ingrese la opcion
    1) Sigues dentro
    2) Salir""")
    print(f"Por vez {count}...")
    opcion = int(input())

print(palabra[::6])

n = 6
m = 8

for i in range(1, n):
    for j in range(m):
        print(f"i:{i} , j:{j}")
i = 0
j = 0
while i < n:
    while j < m:
        print(i*j, end=" ")
        j += 1 
    i +=1 
    j = 0
    print()
